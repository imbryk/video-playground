package com.imbryk.videoapp.presentation.videolist;

import android.os.Bundle;

import com.imbryk.videoapp.presentation.videolist.VideoListView.Listener;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static pl.xsolve.mvp.test.ViewStateTest.simulateConfigurationChange;
import static pl.xsolve.mvp.test.ViewStateTest.simulateFinishConfigChange;
import static pl.xsolve.mvp.test.ViewStateTest.simulateRecreatedFromBackStack;
import static pl.xsolve.mvp.test.ViewStateTest.simulateStart;
import static pl.xsolve.mvp.test.ViewStateTest.simulateStartConfigChange;
import static pl.xsolve.mvp.test.ViewStateTest.verifySingleCallOnView;

@RunWith(MockitoJUnitRunner.class)

public class VideoListViewStateTest {
    private VideoListViewState systemUnderTest;

    @Mock
    private VideoListView view;

    @Before
    public void setUp() throws Exception {
        systemUnderTest = new VideoListViewState();
    }

    @Test
    public void shouldPassMethodsIfHasView() throws Exception {
        systemUnderTest.setView(view);

        systemUnderTest.showNoPermission();
        verifySingleCallOnView(view, () -> verify(view).showNoPermission());

        systemUnderTest.showNoVideos();
        verifySingleCallOnView(view, () -> verify(view).showNoVideos());

        List<VideoViewModel> videoList = someVideosList();
        systemUnderTest.showVideos(videoList);
        verifySingleCallOnView(view, () -> verify(view).showVideos(videoList));

        Listener listener = mock(Listener.class);
        systemUnderTest.setListener(listener);
        verifySingleCallOnView(view, () -> verify(view).setListener(listener));
    }

    @Test
    public void shouldPassVideosReceivedInBackStack() throws Exception {
        List<VideoViewModel> videoList = someVideosList();
        simulateStart(systemUnderTest, view);
        Bundle savedState = simulateStartConfigChange(systemUnderTest, view);

        systemUnderTest.showVideos(videoList);

        verify(view, never()).showVideos(any(List.class));

        simulateFinishConfigChange(systemUnderTest, view, savedState);

        verify(view).showVideos(videoList);
    }

    @Test
    public void shouldKeepCategoriesStateOnConfigurationChange() throws Exception {
        List<VideoViewModel> videoList = someVideosList();
        simulateStart(systemUnderTest, view);
        systemUnderTest.showVideos(videoList);

        simulateConfigurationChange(systemUnderTest, view);

        verify(view).showVideos(videoList);
    }

    @Test
    public void shouldKeepCategoriesWhenRecreatedFromBackStack() throws Exception {
        List<VideoViewModel> videoList = someVideosList();
        simulateStart(systemUnderTest, view);
        systemUnderTest.showVideos(videoList);

        systemUnderTest =
                simulateRecreatedFromBackStack(systemUnderTest, view);

        verify(view).showVideos(videoList);
    }

    @Test
    public void shouldDisplayNoVideosInfoReceivedInBackStack() throws Exception {
        simulateStart(systemUnderTest, view);
        Bundle savedState = simulateStartConfigChange(systemUnderTest, view);

        systemUnderTest.showNoVideos();

        verify(view, never()).showNoVideos();

        simulateFinishConfigChange(systemUnderTest, view, savedState);

        verify(view).showNoVideos();
    }

    @Test
    public void shouldKeepNoVideosInfoOnConfigurationChange() throws Exception {
        simulateStart(systemUnderTest, view);
        systemUnderTest.showNoVideos();

        simulateConfigurationChange(systemUnderTest, view);

        verify(view).showNoVideos();
    }

    @Test
    public void shouldKeepNoVideosInfoWhenRecreatedFromBackStack() throws Exception {
        simulateStart(systemUnderTest, view);
        systemUnderTest.showNoVideos();

        systemUnderTest =
                simulateRecreatedFromBackStack(systemUnderTest, view);

        verify(view).showNoVideos();
    }

    @Test
    public void shouldDisplayNoPermissionInfoReceivedInBackStack() throws Exception {
        simulateStart(systemUnderTest, view);
        Bundle savedState = simulateStartConfigChange(systemUnderTest, view);

        systemUnderTest.showNoPermission();

        verify(view, never()).showNoPermission();

        simulateFinishConfigChange(systemUnderTest, view, savedState);

        verify(view).showNoPermission();
    }

    @Test
    public void shouldKeepNoPermissionInfoOnConfigurationChange() throws Exception {
        simulateStart(systemUnderTest, view);
        systemUnderTest.showNoPermission();

        simulateConfigurationChange(systemUnderTest, view);

        verify(view).showNoPermission();
    }

    @Test
    public void shouldKeepNoPermissionInfoWhenRecreatedFromBackStack() throws Exception {
        simulateStart(systemUnderTest, view);
        systemUnderTest.showNoPermission();

        systemUnderTest =
                simulateRecreatedFromBackStack(systemUnderTest, view);

        verify(view).showNoPermission();
    }

    @Test
    public void shouldPassListenerReceivedInBackStack() throws Exception {
        Listener listener = mock(Listener.class);
        simulateStart(systemUnderTest, view);
        Bundle savedState = simulateStartConfigChange(systemUnderTest, view);

        systemUnderTest.setListener(listener);

        verify(view, never()).setListener(any(Listener.class));

        simulateFinishConfigChange(systemUnderTest, view, savedState);

        verify(view).setListener(listener);
    }

    @Test
    public void shouldKeepListenerOnConfigurationChange() throws Exception {
        Listener listener = mock(Listener.class);
        simulateStart(systemUnderTest, view);
        systemUnderTest.setListener(listener);

        simulateConfigurationChange(systemUnderTest, view);

        verify(view).setListener(listener);
    }

    private List<VideoViewModel> someVideosList() {
        return Arrays.asList(VideoViewModel.builder().setTitle("name").setUri("uri").build());
    }


}
