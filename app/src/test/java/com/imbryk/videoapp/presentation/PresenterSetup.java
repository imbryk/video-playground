package com.imbryk.videoapp.presentation;

import com.imbryk.videoapp.model.UseCaseScheduler;

public class PresenterSetup {
    public static void setScheduler(BasePresenter presenter, UseCaseScheduler useCaseScheduler) {
        presenter.useCaseScheduler = useCaseScheduler;
    }
}
