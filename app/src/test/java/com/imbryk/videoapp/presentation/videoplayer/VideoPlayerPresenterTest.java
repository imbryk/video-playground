package com.imbryk.videoapp.presentation.videoplayer;

import com.imbryk.videoapp.model.InstantTestUseCaseScheduler;
import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.video.GetVideosUseCase;
import com.imbryk.videoapp.presentation.videolist.gateway.VideoListMapper;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.imbryk.videoapp.presentation.PresenterSetup.setScheduler;
import static io.reactivex.Observable.just;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VideoPlayerPresenterTest {
    private VideoPlayerPresenter systemUnderTest;

    @Mock
    private VideoPlayerView view;

    @Mock
    private GetVideosUseCase getVideosUseCase;

    @Mock
    private VideoListMapper videoListMapper;

    private List<VideoItem> videoItems;
    private List<VideoViewModel> videoViewModels;

    @Before
    public void setUp() throws Exception {
        systemUnderTest = new VideoPlayerPresenter();
        systemUnderTest.getVideosUseCase = getVideosUseCase;
        systemUnderTest.videoListMapper = videoListMapper;
        setScheduler(systemUnderTest, new InstantTestUseCaseScheduler());
        initializeDefaults();
        startPresenter();
    }

    private void initializeDefaults() {
        videoItems = someVideoItems();
        videoViewModels = someVideoViewModels();
        simulateVideosListCanBeLoaded();
        simulateItemsCanBeMapped();
    }

    private void startPresenter() {
        systemUnderTest.setView(view);
        systemUnderTest.onStart();
    }

    @Test
    public void shouldPrepareViewWenVideoWasSelected() throws Exception {
        systemUnderTest.playVideo(2);

        verify(view).prepare();
    }

    @Test
    public void shouldPassVideosListAndItemIdToPlayerWhenItemWasSelected() throws Exception {
        systemUnderTest.playVideo(2);
        InOrder inOrder = inOrder(view);
        inOrder.verify(view).setVideos(videoViewModels);
        inOrder.verify(view).playVideo(2);
    }

    private void simulateVideosListCanBeLoaded() {
        when(getVideosUseCase.execute()).thenReturn(just(videoItems));
    }

    private List<VideoItem> someVideoItems() {
        List<VideoItem> items = new ArrayList<>();
        items.add(new VideoItem("name", "uri", "mime"));
        return items;
    }

    private List<VideoViewModel> someVideoViewModels() {
        List<VideoViewModel> items = new ArrayList<>();
        items.add(VideoViewModel.builder().setTitle("name").setUri("uri").build());
        return items;
    }

    private void simulateItemsCanBeMapped() {
        when(videoListMapper.from(videoItems))
                .thenReturn(videoViewModels);
    }
}
