package com.imbryk.videoapp.presentation.videolist;


import com.imbryk.videoapp.model.InstantTestUseCaseScheduler;
import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.capabilities.exception.VideoLoadingException;
import com.imbryk.videoapp.model.domain.video.GetVideosUseCase;
import com.imbryk.videoapp.model.system.GetPermissionsUseCase;
import com.imbryk.videoapp.model.system.capabilities.PermissionDeniedException;
import com.imbryk.videoapp.presentation.videolist.VideoListView.Listener;
import com.imbryk.videoapp.presentation.videolist.gateway.VideoListMapper;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.imbryk.videoapp.presentation.PresenterSetup.setScheduler;
import static io.reactivex.Observable.empty;
import static io.reactivex.Observable.error;
import static io.reactivex.Observable.just;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VideoListPresenterTest {
    private VideoListPresenter systemUnderTest;

    @Mock
    private VideoListView view;

    @Mock
    private GetPermissionsUseCase getPermissionsUseCase;

    @Mock
    private GetVideosUseCase getVideosUseCase;

    @Mock
    private VideoListMapper videoListMapper;

    @Mock
    private VideoListPresenter.Host host;

    private List<VideoItem> videoItems;
    private List<VideoViewModel> videoViewModels;


    @Before
    public void setUp() throws Exception {
        systemUnderTest = new VideoListPresenter();
        systemUnderTest.getPermissionsUseCase = getPermissionsUseCase;
        systemUnderTest.getVideosUseCase = getVideosUseCase;
        systemUnderTest.videoListMapper = videoListMapper;
        setScheduler(systemUnderTest, new InstantTestUseCaseScheduler());
        initializeDefaults();
    }

    private void initializeDefaults() {
        videoItems = someVideoItems();
        videoViewModels = someVideoViewModels();
        simulatePermissionWillNotBeGranted();
        simulateVideosListCanNotBeLoaded();
        simulateItemsCanBeMapped();
    }

    private void startPresenter() {
        systemUnderTest.setView(view);
        systemUnderTest.onStart();
    }

    @Test
    public void shouldAskForPermissionOnStart() throws Exception {
        startPresenter();

        verify(getPermissionsUseCase).execute();
    }

    @Test
    public void shouldNotifyViewIfPermissionWasNotGranted() throws Exception {
        simulatePermissionWillNotBeGranted();
        startPresenter();

        verify(view).showNoPermission();
    }

    @Test
    public void shouldAskForVideosIfPermissionWasGranted() throws Exception {
        simulatePermissionWillBeGranted();
        startPresenter();

        verify(getVideosUseCase).execute();
    }

    @Test
    public void shouldNotifyViewIfVideosCannotBeLoaded() throws Exception {
        simulatePermissionWillBeGranted();
        simulateVideosListCanNotBeLoaded();
        startPresenter();

        verify(view).showNoVideos();
    }

    @Test
    public void shouldNotifyViewIfVideosWereLoaded() throws Exception {
        simulatePermissionWillBeGranted();
        simulateVideosListCanBeLoaded();
        startPresenter();

        verify(view).showVideos(videoViewModels);
    }

    @Test
    public void shouldNotifyHostWhenItemWasSelected() throws Exception {
        final int somePosition = 12;
        startPresenter();
        systemUnderTest.setHost(host);

        ArgumentCaptor<Listener> captor = forClass(Listener.class);
        verify(view).setListener(captor.capture());
        Listener listener = captor.getValue();

        listener.onItemSelected(somePosition);

        verify(host).displayItem(somePosition);
    }

    private void simulatePermissionWillNotBeGranted() {
        when(getPermissionsUseCase.execute()).thenReturn(error(new PermissionDeniedException()));
    }

    private void simulatePermissionWillBeGranted() {
        when(getPermissionsUseCase.execute()).thenReturn(empty());
    }

    private void simulateVideosListCanBeLoaded() {
        when(getVideosUseCase.execute()).thenReturn(just(videoItems));
    }

    private void simulateVideosListCanNotBeLoaded() {
        when(getVideosUseCase.execute()).thenReturn(error(new VideoLoadingException()));
    }

    private List<VideoItem> someVideoItems() {
        List<VideoItem> items = new ArrayList<>();
        items.add(new VideoItem("name", "uri", "mime"));
        return items;
    }

    private List<VideoViewModel> someVideoViewModels() {
        List<VideoViewModel> items = new ArrayList<>();
        items.add(VideoViewModel.builder().setTitle("name").setUri("uri").build());
        return items;
    }

    private void simulateItemsCanBeMapped() {
        when(videoListMapper.from(videoItems))
                .thenReturn(videoViewModels);
    }
}
