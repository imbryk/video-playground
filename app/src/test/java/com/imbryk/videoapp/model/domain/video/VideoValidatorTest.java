package com.imbryk.videoapp.model.domain.video;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;

import org.junit.Before;
import org.junit.Test;

import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_H263;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_H264;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_H265;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_MP4;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_WEBM;
import static org.assertj.core.api.Assertions.assertThat;

public class VideoValidatorTest {

    private VideoValidator systemUnderTest;

    @Before
    public void setUp() throws Exception {
        systemUnderTest = new VideoValidator();
    }

    @Test
    public void shouldValidateValidFormats() throws Exception {
        assertThat(systemUnderTest.isValid(videoItem(VIDEO_H263))).isTrue();
        assertThat(systemUnderTest.isValid(videoItem(VIDEO_H264))).isTrue();
        assertThat(systemUnderTest.isValid(videoItem(VIDEO_H265))).isTrue();
        assertThat(systemUnderTest.isValid(videoItem(VIDEO_MP4))).isTrue();
        assertThat(systemUnderTest.isValid(videoItem(VIDEO_WEBM))).isTrue();
    }

    @Test
    public void shouldMarkInvalidFormats() throws Exception {
        assertThat(systemUnderTest.isValid(videoItem("audio/mp3"))).isFalse();
        assertThat(systemUnderTest.isValid(videoItem("video/avi"))).isFalse();

    }

    private VideoItem videoItem(String mime) {
        return new VideoItem("name", "uri", mime);
    }
}
