package com.imbryk.videoapp.model;

import io.reactivex.Observable;

public class InstantTestUseCaseScheduler implements UseCaseScheduler {
    @Override
    public <T> Observable<T> execute(UseCase<T> useCase) {
        return useCase.execute();
    }
}
