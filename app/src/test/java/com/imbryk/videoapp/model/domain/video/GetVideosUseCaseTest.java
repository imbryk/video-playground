package com.imbryk.videoapp.model.domain.video;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.capabilities.exception.VideoLoadingException;
import com.imbryk.videoapp.model.domain.capabilities.repo.VideoRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetVideosUseCaseTest {
    public static final String KNOWN_MIME = "video/mp4";
    public static final String UNKNOWN_MIME = "video/avi";
    private GetVideosUseCase systemUnderTest;

    @Mock
    private VideoRepository videoRepository;

    @Mock
    private VideoValidator videoValidator;

    private List<VideoItem> videoItems;

    @Before
    public void setUp() throws Exception {
        systemUnderTest = new GetVideosUseCase(videoRepository, videoValidator);
        videoItems = someVideoItems();
        simulateWorkingVideoValidator();
    }


    @Test
    public void shouldReturnVideosFromRepository() throws Exception {
        simulateVideosCanBeLoaded(videoItems);

        Observable<List<VideoItem>> observable = systemUnderTest.execute();

        TestObserver<List<VideoItem>> test = observable.test();
        test.assertSubscribed();
        test.assertValue(videoItems);
    }

    @Test
    public void shouldReturnOnlyKnownVideoTypes() throws Exception {
        List<VideoItem> items = itemsContainingUnknownTypes();
        simulateVideosCanBeLoaded(items);

        Observable<List<VideoItem>> observable = systemUnderTest.execute();

        TestObserver<List<VideoItem>> test = observable.test();
        List<VideoItem> values = test.values().get(0);

        values.forEach(videoItem -> assertThat(videoItem.mime).isEqualTo(KNOWN_MIME));
    }

    @Test
    public void shouldThrowExceptionsIfVideosCannotBeLoaded() throws Exception {
        simulateVideosCanNotBeLoaded();

        Observable<List<VideoItem>> observable = systemUnderTest.execute();

        TestObserver<List<VideoItem>> test = observable.test();
        test.assertError(VideoLoadingException.class);
    }

    @Test
    public void shouldThrowExceptionsIfAllVideosAreUnknownTypes() throws Exception {
        List<VideoItem> items = itemsOfUnknownTypes();
        simulateVideosCanBeLoaded(items);


        Observable<List<VideoItem>> observable = systemUnderTest.execute();

        TestObserver<List<VideoItem>> test = observable.test();
        test.assertError(VideoLoadingException.class);
    }

    @Test
    public void shouldKeepPreviousResult() throws Exception {
        simulateVideosWereAlreadyLoaded(videoItems);


        Observable<List<VideoItem>> observable = systemUnderTest.execute();

        TestObserver<List<VideoItem>> test = observable.test();
        test.assertSubscribed();
        test.assertValue(videoItems);

        verify(videoRepository, times(1)).getVideoItems();
    }

    private void simulateVideosWereAlreadyLoaded(List<VideoItem> items) {
        simulateVideosCanBeLoaded(items);
        Observable<List<VideoItem>> observable = systemUnderTest.execute();

        TestObserver<List<VideoItem>> test = observable.test();
        test.assertSubscribed();
        test.assertValue(items);
    }

    private void simulateWorkingVideoValidator() {
        when(videoValidator.isValid(any(VideoItem.class))).thenAnswer(invocation -> {
            VideoItem item = (VideoItem) invocation.getArguments()[0];
            return KNOWN_MIME.equals(item.mime);
        });
    }

    private void simulateVideosCanNotBeLoaded() {
        when(videoRepository.getVideoItems()).thenReturn(emptyList());
    }

    private void simulateVideosCanBeLoaded(List<VideoItem> videoItems) {
        when(videoRepository.getVideoItems()).thenReturn(videoItems);
    }

    private List<VideoItem> someVideoItems() {
        List<VideoItem> items = new ArrayList<>();
        items.add(new VideoItem("name1", "uri", KNOWN_MIME));
        items.add(new VideoItem("name2", "uri", KNOWN_MIME));
        return items;
    }

    private List<VideoItem> itemsContainingUnknownTypes() {
        List<VideoItem> items = new ArrayList<>();
        items.add(new VideoItem("known", "uri", KNOWN_MIME));
        items.add(new VideoItem("unknown", "uri", UNKNOWN_MIME));
        return items;
    }

    private List<VideoItem> itemsOfUnknownTypes() {
        List<VideoItem> items = new ArrayList<>();
        items.add(new VideoItem("unknown1", "uri", UNKNOWN_MIME));
        items.add(new VideoItem("unknown2", "uri", UNKNOWN_MIME));
        return items;
    }
}
