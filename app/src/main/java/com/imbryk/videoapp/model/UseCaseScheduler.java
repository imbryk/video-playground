package com.imbryk.videoapp.model;

import io.reactivex.Observable;

public interface UseCaseScheduler {
    <T> Observable<T> execute(UseCase<T> useCase);
}
