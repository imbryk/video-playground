package com.imbryk.videoapp.model.system.capabilities;

import com.imbryk.videoapp.model.FlowException;

public class PermissionDeniedException extends FlowException {
}
