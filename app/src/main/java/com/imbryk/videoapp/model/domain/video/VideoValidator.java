package com.imbryk.videoapp.model.domain.video;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;

import java.util.Arrays;
import java.util.List;

import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_H263;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_H264;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_H265;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_MP4;
import static com.google.android.exoplayer2.util.MimeTypes.VIDEO_WEBM;

public class VideoValidator {
    private List<String> validFormats;

    public VideoValidator() {
        generateValidFormatsList();
    }

    private void generateValidFormatsList() {
        validFormats = Arrays.asList(
                VIDEO_MP4,
                VIDEO_WEBM,
                VIDEO_H263,
                VIDEO_H264,
                VIDEO_H265);
    }

    public boolean isValid(VideoItem item) {
        return validFormats.contains(item.mime);
    }
}
