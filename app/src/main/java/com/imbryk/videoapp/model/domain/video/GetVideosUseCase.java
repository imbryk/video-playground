package com.imbryk.videoapp.model.domain.video;

import com.annimon.stream.Stream;
import com.imbryk.videoapp.model.UseCase;
import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.capabilities.exception.VideoLoadingException;
import com.imbryk.videoapp.model.domain.capabilities.repo.VideoRepository;

import java.util.List;

import io.reactivex.Observable;

import static io.reactivex.Observable.defer;
import static io.reactivex.Observable.error;
import static io.reactivex.Observable.just;
import static java.util.Collections.emptyList;

public class GetVideosUseCase implements UseCase<List<VideoItem>> {
    private final VideoRepository videoRepository;
    private final VideoValidator validator;
    private List<VideoItem> videosList = emptyList();

    public GetVideosUseCase(VideoRepository videoRepository, VideoValidator validator) {
        this.videoRepository = videoRepository;
        this.validator = validator;

    }

    @Override
    public Observable<List<VideoItem>> execute() {
        if (hasPreviousResult()) {
            return just(videosList);
        } else {
            return defer(() -> {
                videosList = getVideos();
                if (videosList.size() > 0) {
                    return just(videosList);
                } else {
                    return error(new VideoLoadingException());
                }
            });
        }
    }

    private boolean hasPreviousResult() {
        return videosList.size() > 0;
    }

    private List<VideoItem> getVideos() {
        List<VideoItem> allItems = videoRepository.getVideoItems();

        return Stream.of(allItems)
                .filter(validator::isValid)
                .toList();
    }
}
