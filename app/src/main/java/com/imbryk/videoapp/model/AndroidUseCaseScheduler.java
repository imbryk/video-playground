package com.imbryk.videoapp.model;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class AndroidUseCaseScheduler implements UseCaseScheduler {
    @Override
    public <T> Observable<T> execute(UseCase<T> useCase) {
        return useCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
