package com.imbryk.videoapp.model.system;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.imbryk.videoapp.R;
import com.imbryk.videoapp.model.UseCase;
import com.imbryk.videoapp.model.system.capabilities.PermissionDeniedException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class GetPermissionsUseCase implements UseCase<Void> {
    private static final int PERMISSION_REQUEST = 123;
    private final Activity activity;
    private ObservableEmitter<Void> observableEmitter;

    public GetPermissionsUseCase(Activity activity) {
        this.activity = activity;
    }

    @Override
    public Observable<Void> execute() {
        return Observable.create(observableEmitter -> {
            this.observableEmitter = observableEmitter;
            obtainPermission();
        });
    }

    private void obtainPermission() {
        if (permissionAlreadyGranted()) {
            if (shouldShowRequestRationale()) {
                showRequestPermissionRationale();
            } else {
                requestPermission();
            }
        } else {
            dispatchPermissionGranted();
        }
    }

    private boolean permissionAlreadyGranted() {
        return ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED;
    }

    private boolean shouldShowRequestRationale() {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, READ_EXTERNAL_STORAGE);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(activity, new String[]{READ_EXTERNAL_STORAGE},
                PERMISSION_REQUEST);
    }

    private void showRequestPermissionRationale() {
        runOnMainThread(() -> new AlertDialog.Builder(activity)
                .setMessage(R.string.access_storage_rationale)
                .setPositiveButton(R.string.access_storage_rationale_ok, (dialog, which) -> requestPermission())
                .setNegativeButton(R.string.access_storage_rationale_no, (dialog, which) -> dispatchPermissionNotGranted())
                .setCancelable(false)
                .create()
                .show()
        );

    }

    private void runOnMainThread(Runnable runnable) {
        Handler handler = new Handler(activity.getMainLooper());
        handler.post(runnable);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST) {
            if (permissionGranted(grantResults)) {
                dispatchPermissionGranted();
            } else if (shouldShowRequestRationale()) {
                showRequestPermissionRationale();
            } else {
                dispatchPermissionNotGranted();
            }
        }
    }

    private boolean permissionGranted(int[] grantResults) {
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    private void dispatchPermissionGranted() {
        observableEmitter.onComplete();
    }


    private void dispatchPermissionNotGranted() {
        observableEmitter.onError(new PermissionDeniedException());
    }
}
