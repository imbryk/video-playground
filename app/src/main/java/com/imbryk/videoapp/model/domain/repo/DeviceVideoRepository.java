package com.imbryk.videoapp.model.domain.repo;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.MediaColumns;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.capabilities.repo.VideoRepository;

import org.chalup.microorm.MicroOrm;
import org.chalup.microorm.annotations.Column;

import java.util.ArrayList;
import java.util.List;

import static android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
import static java.util.Collections.emptyList;

public class DeviceVideoRepository implements VideoRepository {

    private final Context context;
    private final MicroOrm microOrm;

    public DeviceVideoRepository(Context context) {
        this.context = context;
        this.microOrm = new MicroOrm();
    }

    @Override
    public List<VideoItem> getVideoItems() {
        Cursor cursor = getItemsCursor();
        if (cursor != null && cursor.moveToFirst()) {
            return readItems(cursor);
        } else {
            return emptyList();
        }
    }

    private Cursor getItemsCursor() {
        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.query(EXTERNAL_CONTENT_URI, null, null, null, MediaColumns.TITLE);
    }

    private List<VideoItem> readItems(Cursor cursor) {
        List<VideoItem> items = new ArrayList<>();

        do {
            VideoRow videoRow = microOrm.fromCursor(cursor, VideoRow.class);
            Uri contentUri = ContentUris.withAppendedId(
                    EXTERNAL_CONTENT_URI, videoRow.id
            );
            items.add(new VideoItem(videoRow.title, contentUri.toString(), videoRow.mime));
        } while (cursor.moveToNext());

        return items;
    }

    public static class VideoRow {
        @Column(MediaColumns._ID)
        long id;

        @Column(MediaColumns.MIME_TYPE)
        public String mime;

        @Column(MediaColumns.TITLE)
        public String title;

        @Column(MediaColumns.DISPLAY_NAME)
        public String name;
    }
}
