package com.imbryk.videoapp.model.domain.capabilities.dto;

public class VideoItem {
    public final String title;
    public final String uri;
    public final String mime;

    public VideoItem(String title, String uri, String mime) {
        this.title = title;
        this.uri = uri;
        this.mime = mime;
    }
}
