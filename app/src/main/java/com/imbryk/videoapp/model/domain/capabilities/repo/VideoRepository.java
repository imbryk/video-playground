package com.imbryk.videoapp.model.domain.capabilities.repo;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;

import java.util.List;

public interface VideoRepository {
    List<VideoItem> getVideoItems();
}
