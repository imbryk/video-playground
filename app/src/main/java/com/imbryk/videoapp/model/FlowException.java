package com.imbryk.videoapp.model;

public abstract class FlowException extends Exception {
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
