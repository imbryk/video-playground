package com.imbryk.videoapp.model;

import io.reactivex.Observable;

public interface UseCase<T> {
    Observable<T> execute();
}
