package com.imbryk.videoapp.android;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.imbryk.videoapp.R;
import com.imbryk.videoapp.dagger.AppComponentFactory;
import com.imbryk.videoapp.dagger.DaggerMainComponent;
import com.imbryk.videoapp.dagger.MainComponent;
import com.imbryk.videoapp.dagger.SystemModule;
import com.imbryk.videoapp.model.system.GetPermissionsUseCase;
import com.imbryk.videoapp.presentation.videolist.VideoListPresenter;
import com.imbryk.videoapp.presentation.videolist.VideoListViewImpl;
import com.imbryk.videoapp.presentation.videolist.VideoListViewState;
import com.imbryk.videoapp.presentation.videoplayer.VideoPlayerPresenter;
import com.imbryk.videoapp.presentation.videoplayer.VideoPlayerViewImpl;
import com.imbryk.videoapp.presentation.videoplayer.VideoPlayerViewState;

import javax.inject.Inject;

import butterknife.BindView;
import pl.xsolve.mvp.api.MvpPresenter;
import pl.xsolve.mvp.api.MvpViewState;
import pl.xsolve.mvp.dagger.MvpActivityComponent;

public class MainActivity extends BaseActivity {

    @Inject
    @MvpPresenter(viewState = "videoListViewState")
    VideoListPresenter videoListPresenter;

    @Inject
    @MvpViewState
    VideoListViewState videoListViewState;

    @Inject
    @MvpPresenter(viewState = "videoPlayerViewState")
    VideoPlayerPresenter videoPlayerPresenter;

    @Inject
    @MvpViewState
    VideoPlayerViewState videoPlayerViewState;

    @Inject
    GetPermissionsUseCase getPermissionsUseCase;

    @BindView(R.id.root)
    View root;

    private VideoPlayerViewImpl videoPlayerView;

    @Override
    protected MainComponent createComponent() {
        return DaggerMainComponent.builder()
                .appComponent(new AppComponentFactory().get())
                .systemModule(new SystemModule(this))
                .build();
    }

    @Override
    protected void inject(MvpActivityComponent component) {
        ((MainComponent) component).inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVideoList();
        initVideoPlayer();
        listenForVideoListClicks();
    }

    private void initVideoList() {
        VideoListViewImpl videoListView = new VideoListViewImpl(root);
        videoListViewState.setView(videoListView);
    }

    private void initVideoPlayer() {
        videoPlayerView = new VideoPlayerViewImpl(root);
        videoPlayerViewState.setView(videoPlayerView);
    }

    private void listenForVideoListClicks() {
        videoListPresenter.setHost(this::displayVideoItem);
    }

    private void displayVideoItem(int position) {
        videoPlayerPresenter.playVideo(position);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getPermissionsUseCase.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoPlayerView.destroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        videoPlayerView.saveState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        videoPlayerView.restoreState(savedInstanceState);
    }
}
