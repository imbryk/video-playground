package com.imbryk.videoapp.android;

import android.os.Bundle;

import butterknife.ButterKnife;
import pl.xsolve.mvp.MvpActivity;

public abstract class BaseActivity extends MvpActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflate();
    }

    private void inflate() {
        setContentView(getLayoutResourceId());
        bindView();
    }

    protected abstract int getLayoutResourceId();

    private void bindView() {
        ButterKnife.bind(this);
    }
}
