package com.imbryk.videoapp.android;

import android.app.Application;

import com.imbryk.videoapp.dagger.AppComponentFactory;


public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        new AppComponentFactory().create(this);
    }
}
