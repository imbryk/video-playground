package com.imbryk.videoapp.dagger;

import android.content.Context;

import com.imbryk.videoapp.model.domain.capabilities.repo.VideoRepository;
import com.imbryk.videoapp.model.domain.repo.DeviceVideoRepository;
import com.imbryk.videoapp.model.domain.video.GetVideosUseCase;
import com.imbryk.videoapp.model.domain.video.VideoValidator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class VideoModule {


    @Singleton
    @Provides
    public VideoRepository provideVideoRepository(Context context) {
        return new DeviceVideoRepository(context);
    }

    @Singleton
    @Provides
    public VideoValidator provideVideoValidator() {
        return new VideoValidator();
    }
    @Singleton
    @Provides
    public GetVideosUseCase provideGetVideosUseCase(VideoRepository videoRepository, VideoValidator validator) {
        return new GetVideosUseCase(videoRepository, validator);
    }

}
