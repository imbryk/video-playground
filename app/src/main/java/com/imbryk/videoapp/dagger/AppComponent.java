package com.imbryk.videoapp.dagger;

import android.content.Context;

import com.imbryk.videoapp.model.UseCaseScheduler;
import com.imbryk.videoapp.model.domain.video.GetVideosUseCase;

import javax.inject.Singleton;

import dagger.Component;
import pl.xsolve.mvp.dagger.MvpAppModule;


@Singleton
@Component(
        modules = {
                MvpAppModule.class,
                AppModule.class,
                VideoModule.class,
        }
)
public interface AppComponent {
    Context appContext();

    GetVideosUseCase provideGetVideosUseCase();

    UseCaseScheduler provideUseCaseScheduler();

}
