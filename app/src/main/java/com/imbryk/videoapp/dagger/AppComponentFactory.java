package com.imbryk.videoapp.dagger;

import android.app.Application;

import pl.xsolve.mvp.dagger.MvpAppModule;


public class AppComponentFactory {
    private static AppComponent sAppComponent;

    public AppComponentFactory() {
    }

    public AppComponent create(Application app) {
        if (sAppComponent == null) {
            sAppComponent = DaggerAppComponent.builder().
                    mvpAppModule(new MvpAppModule(app.getApplicationContext())).build();
        }

        return sAppComponent;
    }

    public AppComponent get() {
        if (sAppComponent == null) {
            throw new RuntimeException("call create from application before calling get()");
        } else {
            return sAppComponent;
        }
    }
}
