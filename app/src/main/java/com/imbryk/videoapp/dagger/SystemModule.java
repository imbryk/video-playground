package com.imbryk.videoapp.dagger;

import android.app.Activity;

import com.imbryk.videoapp.model.system.GetPermissionsUseCase;

import dagger.Module;
import dagger.Provides;
import pl.xsolve.mvp.dagger.scope.ScreenScope;

@Module
public class SystemModule {

    private final Activity activity;

    public SystemModule(Activity activity) {
        this.activity = activity;
    }

    @ScreenScope
    @Provides
    public GetPermissionsUseCase provideGetPermissionsUseCase() {
        return new GetPermissionsUseCase(activity);
    }
}
