package com.imbryk.videoapp.dagger;

import com.imbryk.videoapp.model.AndroidUseCaseScheduler;
import com.imbryk.videoapp.model.UseCaseScheduler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule {

    @Singleton
    @Provides
    public UseCaseScheduler provideUseCaseScheduler() {
        return new AndroidUseCaseScheduler();
    }

}
