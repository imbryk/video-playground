package com.imbryk.videoapp.dagger;

import com.imbryk.videoapp.android.MainActivity;

import dagger.Component;
import pl.xsolve.mvp.dagger.MvpActivityComponent;
import pl.xsolve.mvp.dagger.MvpActivityModule;
import pl.xsolve.mvp.dagger.scope.ScreenScope;

@ScreenScope
@Component(
        dependencies = {
                AppComponent.class
        },
        modules = {
                MvpActivityModule.class,
                SystemModule.class
        }
)
public interface MainComponent extends MvpActivityComponent {
    void inject(MainActivity activity);
}
