package com.imbryk.videoapp.presentation.videolist.gateway;

import com.annimon.stream.Stream;
import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

import javax.inject.Inject;

public class VideoListMapper {

    @Inject
    public VideoListMapper() {
    }

    public List<VideoViewModel> from(List<VideoItem> items) {
        return Stream.of(items)
                .map(videoItem -> VideoViewModel.builder()
                        .setTitle(videoItem.title)
                        .setUri(videoItem.uri)
                        .build())
                .toList();
    }
}
