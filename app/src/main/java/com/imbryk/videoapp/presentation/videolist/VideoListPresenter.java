package com.imbryk.videoapp.presentation.videolist;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.video.GetVideosUseCase;
import com.imbryk.videoapp.model.system.GetPermissionsUseCase;
import com.imbryk.videoapp.presentation.BasePresenter;
import com.imbryk.videoapp.presentation.videolist.gateway.VideoListMapper;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import pl.xsolve.mvp.dagger.scope.ScreenScope;

@ScreenScope
public class VideoListPresenter extends BasePresenter<VideoListView> {

    @Inject
    GetVideosUseCase getVideosUseCase;

    @Inject
    GetPermissionsUseCase getPermissionsUseCase;

    @Inject
    VideoListMapper videoListMapper;

    private Disposable permissionsSubscription;
    private Disposable videosSubscription;
    private Host host;

    @Inject
    public VideoListPresenter() {
    }

    @Override
    public void onStart() {
        super.onStart();
        getPermissions();
    }

    @Override
    public void setView(VideoListView videoListView) {
        super.setView(videoListView);
        view.setListener(this::itemSelected);
    }

    private void itemSelected(int position) {
        host.displayItem(position);
    }

    @Override
    public void onFinish() {
        super.onFinish();
        permissionsSubscription.dispose();
        videosSubscription.dispose();
    }

    private void getPermissions() {
        permissionsSubscription = useCaseScheduler.execute(getPermissionsUseCase)
                .subscribe(aVoid -> {
                }, this::permissionDenied, this::permissionReceived);

    }

    private void permissionReceived() {
        loadVideoList();
    }

    private void permissionDenied(Throwable throwable) {
        view.showNoPermission();
    }

    private void loadVideoList() {
        videosSubscription = useCaseScheduler.execute(getVideosUseCase)
                .subscribe(this::videosReceived, this::errorLoadingVideos);
    }

    private void videosReceived(List<VideoItem> videoItems) {
        view.showVideos(videoListMapper.from(videoItems));
    }

    private void errorLoadingVideos(Throwable throwable) {
        view.showNoVideos();
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public interface Host {
        void displayItem(int position);
    }
}
