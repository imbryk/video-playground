package com.imbryk.videoapp.presentation.videolist.model;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class VideoViewModel implements Parcelable {
    public static final VideoViewModel NULL = builder().setTitle("").setUri("").build();

    public abstract String title();

    public abstract String uri();

    public static Builder builder() {
        return new AutoValue_VideoViewModel.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setTitle(String value);

        public abstract Builder setUri(String value);

        public abstract VideoViewModel build();
    }
}
