package com.imbryk.videoapp.presentation.videolist;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.imbryk.videoapp.R;
import com.imbryk.videoapp.presentation.videolist.VideosListAdapter.OnItemClickedListener;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.video_item_title)
    TextView videoTitle;

    @BindView(R.id.video_item_thumb)
    ImageView videoThumb;

    private OnItemClickedListener onClickListener;
    private int position = 0;

    public VideoItemViewHolder(View itemView, OnItemClickedListener onClickListener) {
        super(itemView);
        this.onClickListener = onClickListener;
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this::itemClicked);
    }

    public void bind(VideoViewModel viewModel, int position) {
        this.position = position;
        videoTitle.setText(viewModel.title());
        displayImage(viewModel.uri());
    }

    private void displayImage(String uri) {
        Uri thumbUri = Uri.parse(uri);
        Glide.with(videoThumb.getContext())
                .load(thumbUri)
                .centerCrop()
                .into(videoThumb);
    }

    private void itemClicked(View view) {
        onClickListener.onItemClicked(position);
    }
}
