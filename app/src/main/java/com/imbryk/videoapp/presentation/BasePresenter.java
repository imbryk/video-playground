package com.imbryk.videoapp.presentation;

import com.imbryk.videoapp.model.UseCaseScheduler;

import javax.inject.Inject;

import pl.xsolve.mvp.Presenter;

public class BasePresenter<VIEW> extends Presenter<VIEW> {
    @Inject
    protected UseCaseScheduler useCaseScheduler;
}
