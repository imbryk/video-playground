package com.imbryk.videoapp.presentation.videolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imbryk.videoapp.R;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

import static java.util.Collections.emptyList;

class VideosListAdapter extends RecyclerView.Adapter<VideoItemViewHolder> {
    private final OnItemClickedListener itemClickedListener;
    private List<VideoViewModel> videos = emptyList();

    VideosListAdapter(OnItemClickedListener itemClickedListener) {
        this.itemClickedListener = itemClickedListener;
    }

    @Override
    public VideoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_item, parent, false);
        return new VideoItemViewHolder(itemView, itemClickedListener);
    }

    @Override
    public void onBindViewHolder(VideoItemViewHolder holder, int position) {
        holder.bind(getItem(position), position);
    }

    private VideoViewModel getItem(int position) {
        return videos.get(position);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public void setData(List<VideoViewModel> videos) {
        this.videos = videos;
        notifyDataSetChanged();
    }

    public interface OnItemClickedListener {
        void onItemClicked(int position);
    }
}
