package com.imbryk.videoapp.presentation.videoplayer;

import com.imbryk.videoapp.model.domain.capabilities.dto.VideoItem;
import com.imbryk.videoapp.model.domain.video.GetVideosUseCase;
import com.imbryk.videoapp.presentation.BasePresenter;
import com.imbryk.videoapp.presentation.videolist.gateway.VideoListMapper;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import pl.xsolve.mvp.dagger.scope.ScreenScope;

@ScreenScope
public class VideoPlayerPresenter extends BasePresenter<VideoPlayerView> {
    @Inject
    GetVideosUseCase getVideosUseCase;

    @Inject
    VideoListMapper videoListMapper;

    private Disposable videosSubscription;

    @Inject
    public VideoPlayerPresenter() {
    }

    public void playVideo(int position) {
        view.prepare();
        loadVideoListAndPlay(position);
    }
    private void loadVideoListAndPlay(int position) {
        videosSubscription = useCaseScheduler.execute(getVideosUseCase)
                .subscribe(videoItems -> {
                    videosReceived(videoItems);
                    playItem(position);
                }, this::errorLoadingVideos);
    }

    private void videosReceived(List<VideoItem> videoItems) {
        List<VideoViewModel> videos = videoListMapper.from(videoItems);
        view.setVideos(videos);
    }

    private void playItem(int position) {
        view.playVideo(position);
    }

    private void errorLoadingVideos(Throwable throwable) {
    }
}
