package com.imbryk.videoapp.presentation.videolist;

import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

public interface VideoListView {
    void showNoPermission();

    void showNoVideos();

    void showVideos(List<VideoViewModel> videos);

    void setListener(Listener listener);

    interface Listener {
        Listener EMPTY = position -> {
        };

        void onItemSelected(int position);
    }
}
