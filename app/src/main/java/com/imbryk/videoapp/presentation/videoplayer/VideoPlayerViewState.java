package com.imbryk.videoapp.presentation.videoplayer;

import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

import javax.inject.Inject;

import pl.xsolve.mvp.ViewState;
import pl.xsolve.mvp.dagger.scope.ScreenScope;

@ScreenScope
public class VideoPlayerViewState extends ViewState<VideoPlayerView> implements VideoPlayerView {

    private boolean isPrepared;

    @Inject
    public VideoPlayerViewState() {
    }

    @Override
    public void setView(VideoPlayerView videoPlayerView) {
        super.setView(videoPlayerView);
        updatedView();
    }

    private void updatedView() {
        if (isPrepared) {
            view.prepare();
        }
    }

    @Override
    public void prepare() {
        isPrepared = true;
        if (hasView()) {
            view.prepare();
        }
    }

    @Override
    public void setVideos(List<VideoViewModel> videos) {
        if (hasView()) {
            view.setVideos(videos);
        }
    }

    @Override
    public void playVideo(int position) {
        if (hasView()) {
            view.playVideo(position);
        }
    }
}
