package com.imbryk.videoapp.presentation.videoplayer;

import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

public interface VideoPlayerView {
    void prepare();

    void setVideos(List<VideoViewModel> videos);

    void playVideo(int position);
}
