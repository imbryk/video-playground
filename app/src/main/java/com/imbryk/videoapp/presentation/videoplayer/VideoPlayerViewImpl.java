package com.imbryk.videoapp.presentation.videoplayer;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import com.annimon.stream.Stream;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.imbryk.videoapp.R;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.media.AudioManager.STREAM_MUSIC;
import static android.view.View.VISIBLE;
import static com.google.android.exoplayer2.ExoPlayerFactory.newSimpleInstance;

public class VideoPlayerViewImpl implements VideoPlayerView {

    private static final String PLAYBACK_POSITION = "VideoPlayerViewImpl.PLAYBACK_POSITION";
    private static final String POSITION = "VideoPlayerViewImpl.POSITION";
    private static final String VIDEOS = "VideoPlayerViewImpl.VIDEOS";
    private final Context context;

    @BindView(R.id.player_container)
    View container;
    @BindView(R.id.player)
    SimpleExoPlayerView exoPlayerView;
    @BindView(R.id.volume)
    SeekBar volumeBar;

    private SimpleExoPlayer exoPlayer;
    private ArrayList<VideoViewModel> videos = new ArrayList<>(); //ArrayList to allow saving to bundle
    private int position;
    private AudioManager audioManager;

    public VideoPlayerViewImpl(View root) {
        context = root.getContext();
        ButterKnife.bind(this, root);
        prepareExoPlayer();
        prepareVolumeBar();
    }

    private void prepareVolumeBar() {
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        volumeBar.setMax(audioManager.getStreamMaxVolume(STREAM_MUSIC));
        int currentVolume = audioManager.getStreamVolume(STREAM_MUSIC);
        volumeBar.setProgress(currentVolume);
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void prepareExoPlayer() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        exoPlayer = newSimpleInstance(context, trackSelector);

        exoPlayerView.setUseController(true);
        exoPlayerView.requestFocus();
        exoPlayerView.setPlayer(exoPlayer);
        exoPlayerView.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                volumeBar.setVisibility(visibility);
                updateVolumeBar();
            }
        });
    }

    private void updateVolumeBar() {
        int currentVolume = audioManager.getStreamVolume(STREAM_MUSIC);
        volumeBar.setProgress(currentVolume);
    }

    @Override
    public void prepare() {
        container.setVisibility(VISIBLE);
    }

    @Override
    public void setVideos(List<VideoViewModel> videos) {
        this.videos = new ArrayList<>(videos);
    }

    @Override
    public void playVideo(int position) {
        this.position = position;
        playVideoList();
    }

    private void playVideoList() {

        MediaSource[] mediaSources = new MediaSource[videos.size()];
        final int[] index = {0};
        Stream.of(videos)
                .forEach(videoViewModel -> {
                    addVideoToMediaSourceAtPosition(videoViewModel, mediaSources, index[0]);
                    index[0] += 1;
                });
        ConcatenatingMediaSource concatenatedSource =
                new ConcatenatingMediaSource(mediaSources);

        LoopingMediaSource loopingMediaSource = new LoopingMediaSource(concatenatedSource);


        exoPlayer.prepare(loopingMediaSource);
        exoPlayer.seekToDefaultPosition(this.position);
        exoPlayer.setPlayWhenReady(true);
    }

    private void addVideoToMediaSourceAtPosition(VideoViewModel videoViewModel, MediaSource[] mediaSources, int position) {
        Uri videoUri = Uri.parse(videoViewModel.uri());


        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
                context,
                Util.getUserAgent(context, context.getString(R.string.app_name)),
                bandwidthMeter);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);

        mediaSources[position] = videoSource;

    }


    private VideoViewModel getVideo(int position) {
        if (videos.size() > position) {
            return videos.get(position);
        } else {
            return VideoViewModel.NULL;
        }
    }

    public void saveState(Bundle outState) {
        outState.putLong(PLAYBACK_POSITION, exoPlayer.getCurrentPosition());
        outState.putInt(POSITION, exoPlayer.getCurrentWindowIndex());
        outState.putParcelableArrayList(VIDEOS, videos);
    }

    public void restoreState(Bundle savedInstanceState) {
        long playbackPosition = savedInstanceState.getLong(PLAYBACK_POSITION);
        videos = savedInstanceState.getParcelableArrayList(VIDEOS);
        position = savedInstanceState.getInt(POSITION);
        playVideoList();
        setPlaybackPosition(playbackPosition);
    }

    private void setPlaybackPosition(long playbackPosition) {
        exoPlayer.seekTo(position, playbackPosition);
    }

    public void destroy() {
        exoPlayer.stop();
        exoPlayer.release();
    }
}
