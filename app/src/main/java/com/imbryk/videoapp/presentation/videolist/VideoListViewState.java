package com.imbryk.videoapp.presentation.videolist;

import android.os.Bundle;

import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pl.xsolve.mvp.ViewState;
import pl.xsolve.mvp.dagger.scope.ScreenScope;

import static com.imbryk.videoapp.presentation.videolist.VideoListView.Listener.EMPTY;

@ScreenScope
public class VideoListViewState extends ViewState<VideoListView> implements VideoListView {

    private static final int STATE_INITIAL = 0;
    private static final int STATE_VIDEOS = 1;
    private static final int STATE_NO_VIDEOS = 2;
    private static final int STATE_NO_PERMISSION = 3;
    private static final int STATE_TOUCHED = 4;

    private static final String STATE = "VideoListViewState.STATE";
    private static final String VIDEOS = "VideoListViewState.VIDEOS";

    private int state = STATE_INITIAL;
    private ArrayList<VideoViewModel> videos = new ArrayList<>(); //ArrayList to allow saving to bundle
    private Listener listener = EMPTY;

    @Inject
    public VideoListViewState() {
    }

    @Override
    public void setView(VideoListView videoListView) {
        super.setView(videoListView);
        updateView();
    }

    private void updateView() {
        switch (state) {
            case STATE_VIDEOS:
                if (hasVideos()) {
                    view.showVideos(videos);
                }
                break;
            case STATE_NO_VIDEOS:
                view.showNoVideos();
                break;
            case STATE_NO_PERMISSION:
                view.showNoPermission();
                break;
            default:
                //no-op
        }
        if (hasListener()) {
            view.setListener(listener);
        }
    }

    private boolean hasVideos() {
        return videos.size() > 0;
    }

    private boolean hasListener() {
        return listener != EMPTY;
    }

    @Override
    public void showNoPermission() {
        state = STATE_NO_PERMISSION;
        if (hasView()) {
            view.showNoPermission();
        }
    }

    @Override
    public void showNoVideos() {
        state = STATE_NO_VIDEOS;
        if (hasView()) {
            view.showNoVideos();
        }
    }

    @Override
    public void showVideos(List<VideoViewModel> videos) {
        this.videos = new ArrayList<>(videos);
        state = STATE_VIDEOS;
        if (hasView()) {
            view.showVideos(videos);
        }
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
        touch();
        if (hasView()) {
            view.setListener(listener);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(VIDEOS, videos);
        outState.putInt(STATE, state);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (!wasTouched()) {
            updateFromSavedState(savedInstanceState);
        }
    }

    private void updateFromSavedState(Bundle savedInstanceState) {
        state = savedInstanceState.getInt(STATE, state);
        if (savedInstanceState.containsKey(VIDEOS)) {
            videos = savedInstanceState.getParcelableArrayList(VIDEOS);
        }
        if (hasView()) {
            updateView();
        }
    }

    private void touch() {
        if (state == STATE_INITIAL) {
            state = STATE_TOUCHED;
        }
    }

    private boolean wasTouched() {
        return state != STATE_INITIAL;
    }
}
