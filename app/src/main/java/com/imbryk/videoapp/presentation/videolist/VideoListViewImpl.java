package com.imbryk.videoapp.presentation.videolist;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.imbryk.videoapp.R;
import com.imbryk.videoapp.presentation.videolist.model.VideoViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoListViewImpl implements VideoListView {

    @BindView(R.id.videos_list)
    RecyclerView videosList;
    @BindView(R.id.notifications)
    TextView notifications;

    private VideosListAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Listener listener = Listener.EMPTY;

    public VideoListViewImpl(View root) {
        ButterKnife.bind(this, root);
        initList(root.getContext());
    }

    private void initList(Context context) {
        layoutManager = new LinearLayoutManager(context);
        adapter = new VideosListAdapter(this::itemClicked);
        videosList.setLayoutManager(layoutManager);
        videosList.setAdapter(adapter);
    }

    private void itemClicked(int position) {
        listener.onItemSelected(position);
    }

    @Override
    public void showNoPermission() {
        notifications.setText(R.string.permission_denied);
        notifications.setVisibility(View.VISIBLE);
        videosList.setVisibility(View.GONE);
    }

    @Override
    public void showNoVideos() {
        notifications.setText(R.string.unable_to_find_videos);
        notifications.setVisibility(View.VISIBLE);
        videosList.setVisibility(View.GONE);
    }

    @Override
    public void showVideos(List<VideoViewModel> videos) {
        adapter.setData(videos);
        notifications.setVisibility(View.GONE);
        videosList.setVisibility(View.VISIBLE);
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
